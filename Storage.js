Storage = {
		types: ['learn', 'repeat', 'known'],
		typeValues: {},
		init: function() {
			var self = this;
			this.types.forEach(function(type) {
				self.typeValues[type] = self._getStorage(type); 
			});
		},
		_getStorage: function(type) {
			return JSON.parse(localStorage.getItem(type) || "[]" );
		},
		_saveStorage: function(type) {
			localStorage.setItem(type, JSON.stringify( this.typeValues[type] ));
		},
		map: function(type) {
			return this.typeValues[type].map(function(o) {return o.name; });
		},
		is: function(type, word) {
			return this.getIndex(type, word)  > -1;
		},
		getIndex: function(type, word) {
			return this.map(type).indexOf( word );
		},
		get: function(type, index) {
			return this.typeValues[type][index];
		},
		add: function(type, word, ex) {
			this.typeValues[type].unshift({
				name: word,
				ex: ex || null,
				date: new Date(),
				count: 0,
				last: new Date()
			});
			this._saveStorage(type);
		},
		edit: function(type, word, obj ) {
			var index = this.getIndex(type, word);
			var val =  this.typeValues[type][index];
			
			if ( val ) {
				for ( var i in obj ) {	
					val[i] = obj[i];
				}
				this.typeValues[type][index] = val;
				this._saveStorage(type);
			}
		},
		remove: function(type, word) {
			var res = null;
			var vals = this.typeValues[type];
			for (var i = 0; i < vals.length; i++ ) {
				if ( vals[i].name == word ) {
					res = vals[i];
					vals.splice(i, 1);
					break;
				}
			}
			this._saveStorage(type);
			return res;
		}
};