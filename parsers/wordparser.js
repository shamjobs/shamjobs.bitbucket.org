/**
 * Created by vasilij on 05.04.17.
 */
var request = require("request");
var cheerio = require("cheerio");


var arrs = [
    { n: "A-B", m: 5},
    { n: "C-D", m: 7},
    { n: "E-G", m: 6},
    { n: "H-K", m: 4},
    { n: "L-N", m: 4},
    { n: "O-P", m: 5},
    { n: "Q-R", m: 3},
    { n: "S", m: 5},
    { n: "T", m: 3},
    { n: "U-Z", m: 3}
];


var url = "http://www.oxfordlearnersdictionaries.com/wordlist/english/oxford3000/Oxford3000_";
var words = [];
var last = arrs.pop();
var lastPage = 1;


var resp = function (error, response, body) {
    if (!error) {
        var $ = cheerio.load(body);
        var items = $("ul.wordlist-oxford3000 > li > a")
        items.each(function (i, el) {
            words.push( $(el).text().trim().toLocaleLowerCase() );
        })
        if (arrs.length > 0 || lastPage < last.m) {
            if ( lastPage > last.m ) {
                last = arrs.pop();
                lastPage = 1;
            } else {
                lastPage++;
            }
            request( url+last.n+"/?page="+lastPage , resp);
            console.log(words.length)
        } else {
            console.log(words.length, JSON.stringify(words));
        }
    } else {
        console.log("Произошла ошибка: " + error);
    }
}
request(url+last.n+"/?page="+lastPage, resp);