/**
 * Created by vasilij on 09/05/17.
 */


var request = require("request");
var cheerio = require("cheerio");

var urls = ["http://studynow.ru/phrases/top/","http://studynow.ru/phrases/intros1/","http://studynow.ru/phrases/intros2/","http://studynow.ru/phrases/questions1/","http://studynow.ru/phrases/questions2/","http://studynow.ru/phrases/answers1/","http://studynow.ru/phrases/request1/","http://studynow.ru/phrases/time1","http://studynow.ru/phrases/time2/","http://studynow.ru/phrases/opinion1/","http://studynow.ru/phrases/descr1/","http://studynow.ru/phrases/char1/","http://studynow.ru/phrases/complicated/","http://studynow.ru/phrases/phrasalverbs1/","http://studynow.ru/phrases/phrasalverbs2/","http://studynow.ru/phrases/phrasalverbs3/","http://studynow.ru/phrases/phrasalverbs4/"];
var words = [];
var resp = function (error, response, body) {
    if (!error) {
        var $ = cheerio.load(body);
        var items = $(".sptable tr td[colspan=2]");
        items.each(function (i, el) {
            var text = $(el).contents().text().trim();
            if (text.indexOf("—")>-1) {
                var en =  text.split("—")[0].trim();
                var ru =  text.split("—")[1].trim();
                words.push( { name: en, ex: ru }  );
            }
        })
        if (urls.length>0) {
            request( urls.shift() , resp);
            console.log(words.length)
        } else {
            console.log(words.length, JSON.stringify(words));
        }
    } else {
        console.log("Произошла ошибка: " + error);
    }
}
request(urls.shift(), resp);
