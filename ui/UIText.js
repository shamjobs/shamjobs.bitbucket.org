UIText = {
		init: function(clickCallback) {
			var text = $("#text");
			$("#textOk").click(function(e) {
				var text = $("#text").val().replace(/\[br\]/g, ' ');
				clickCallback(Analizer.call(text));
			});
		}
}