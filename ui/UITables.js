UITables = {
		types: [ 'words', 'known', 'learn', 'repeat'],
		temp: null,
		index: 0,
		
		init: function(proxyCallback) {
			$(".tableWords tbody").click(function(e) {
				var elem = $(e.target);
				var tr = elem.closest("tr");
				if ( tr ) {
					var to = elem.attr('to');
					var action = elem.attr('action');
					var type = tr.attr('_type'); 
					var word = tr.find("td:nth-child(2)").text();
					
					proxyCallback( word, type, action, to, elem);
					
					if ( action=='up' ) {
						var val = elem.parent().find('b');
						val.text( parseInt(val.text()) + 1 )
					}
					if ( action=='down' ) {
						var val = elem.parent().find('b');
						val.text( parseInt(val.text()) - 1 )
					}
				}
			});
		},
		editEx: function( elem, isEdit, val ) {
			var td = elem.closest("td");
			td.find('button').remove();
			var val = !isEdit ? td.find('input').val() : td.text();  
			td.html( this.exStr(val, isEdit) );	
			return val;		
		},
		exStr: function(ex, edit) { 
			return ( !edit && ex ? ex+"<button  action='editEx'>e</button>" 
				: "<input type='text' value=\""+(ex || "").replace(/"/g,"'")+"\" />" +
						"<button action='saveEx'>s</button>" ); 	
		},
		append: function(type, w, ex, count, num) {
			if (!this.temp) 
				this.temp = "";
			this.temp += "<tr _type='"+type+"'>" +
					"<td width='50'>"+num+"</td>"+
					"<td>"+w+"</td>" +
					"<td style='50%'>" +(type == 'learn' || type == 'repeat' || ex ? this.exStr(ex) : "") + "</td>"+
					'<td width="300" class="bts">'+
					(type == 'repeat' || type=='learn' ? "<b>"+count+"</b> " : "")+
					(type == 'repeat' || type=='learn' ? '<span action="up" class="glyphicon glyphicon-arrow-up"></span>' : "")+ 
					(type == 'repeat' || type=='learn' ? '<span action="down" class="glyphicon glyphicon-arrow-down"></span>' : "")+ 
					(type != 'learn' ? '<span to="learn" action="move" class="glyphicon glyphicon-education"></span>' : "")+ 
					(type != 'repeat' ? '<span to="repeat" action="move" class="glyphicon glyphicon-repeat"></span>' : "")+ 
					(type != 'known' ? '<span to="known" action="move" class="glyphicon glyphicon-ok"></span>' : "")+
					'<a target="_blank" href="http://wooordhunt.ru/word/'+w+'" class="glyphicon glyphicon-question-sign"></span>'+
					'<a target="_blank" href="https://tatoeba.org/rus/sentences/search?query='+encodeURI(w)+'&from=eng&to=rus" class="glyphicon glyphicon-question-sign"></span>'+
					'<a target="_blank" href="http://www.oxfordlearnersdictionaries.com/definition/english/'+encodeURI(w.replace(/\s+/g, '-'))+'" class="glyphicon glyphicon-question-sign"></span>'+
					'</td>' +
					"</tr>";
		},
		
		flush: function(type) {
			var tbl = $("#"+type+" tbody");
			tbl.empty();
			//var doc = $(document.createDocumentFragment())
			tbl.prepend(this.temp);
			this.temp = null;
			this.index = 0;
		}
};