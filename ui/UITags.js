UITags = {
    	text: "",
		init: function( preLoadCallback, callbackClick) {
			$("#tag-button").click(preLoadCallback)
            $("#tags").click(function(e) {
				var tag = $(e.target); 
				var t = tag.attr('val');
				if ( tag.hasClass('btn-info') ) {
					tag.removeClass('btn-info');
					t = null;
                    $("#tag-button").removeClass('btn-danger');
				} else {
                    $("#tag-button").addClass('btn-danger');
				}
				callbackClick(t);
			});
		},
		append: function(tag, size, active) {
			this.text += "<li><a class='tgs "+( active ? "btn-info" : "")+"' href='#' val='"+tag+"'>"+tag+" ("+size+")</a></li>";
		},
		flush: function(tag) {
            $("#tags").html(this.text);
            this.text = "";
		}
};