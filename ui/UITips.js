UITips = {
		init: function(keyUpCallback) {
			$("#hideTips").click(function() {
				if ( $(this).is(':checked') ) {
					$("table td:nth-child(2)").hide();  
				} else {
					$("table td:nth-child(2)").show();
				}
			});
			$( "input" ).keyup(function( event ) {  keyUpCallback($(this).val());});
		},
		update: function( activePage) {
			$("#activePage").text(activePage);
		}
};