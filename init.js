var STEP = 500;

var words =  w5000;
var activeTab = 'words';
var pattern = null;
var ctag = null;

var activePage = 0;


reloadTable = function( activePage ) {
    console.time("all")

	var currTags = ctag ? tags[ctag] : null;
	var currPattern = pattern ? new RegExp('^'+pattern.replace(/\*/g,'.*')+'$') : null;
	console.log(currPattern, pattern)
	var all = 0;
	var counts = {};
	var res = [];
    var setWords = {};

	console.time("filtering")
	Storage.types.forEach(function(t) { 
		counts[t] = 0;
		Storage.typeValues[t].forEach(function(val, i) {
			setWords[val.name] = 1;
			if ( (!currPattern || currPattern.test(val.name))
					&& ( currTags==null || currTags.indexOf(val.name) > -1 )) {
				if ( activeTab == t) {
					res.push(val);
				}
				counts[t] += 1;
				all++;
			}
		});
	} ) ;
	counts['words'] = 0;
	var date = new Date();
	for ( var i = 0; i < words.length; i++ ) {
		var word = words[i];
		var ex = null;
		var w = null;
		if ( word != null && typeof word == 'object') {
			w = word.name;
			ex = word.ex;
		}  else {
			w = word.toLowerCase();
		}
		if ( (!currPattern || currPattern.test(w))
				&& !setWords[w]
				&& ( currTags==null || currTags.indexOf(w) > -1 )) {
			if ( activeTab == 'words' ) {
				res.push({ name: w, ex: ex, date: date, count: i});
			}
			counts['words'] += 1;
			all++;
		}
	}
	console.timeEnd("filtering")
	
	console.time("sorts");
	//console.log(activePage)
    //activePage = activePage < 0 ? 0 : ((activePage + 1) * STEP > res.length ) ? activePage - 1 : activePage;
    var currMin = activePage * STEP;
    var currMax = (activePage + 1) * STEP;

    UITips.update( currMin +"-"+currMax )
	res.sort(function(a, b ) {
		var c = a.count - b.count;
		return  c!=0 ? c : new Date(b.date).getTime() - new Date(a.date).getTime()  ; 		
	}).forEach(function(v, i) {
		if (i >=currMin && i < currMax)
			UITables.append(activeTab, v.name, v.ex, v.count, i+1);
	})
	console.timeEnd("sorts")	
	
	console.time("flush")
    for ( var i in counts ) $(".nav-tabs a[href='#"+i+"Tab']").text(i+" ( "+ counts[i] +" )");

	UITables.flush(activeTab);
	console.timeEnd("flush")

	console.timeEnd("all")
	return activePage;
}


$( document ).ready(function() {
	$('select#type').change(function(e){
		var val = $('select#type option:selected').val();
		switch (val) {
			case "w5000": words = w5000; break;
			case "ox3000": words = ox3000; break;
			case "pv": words = pv200; break;
            case "phrases300": words = phrases300; break;
			default:break;
		}
		activePage = reloadTable(0);
	});
	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {  activeTab = $(this).attr('aria-controls'); activePage = reloadTable(0); });
	$( "#nextPage" ).click(function() { activePage = reloadTable(++activePage); });
	$( "#prevPage" ).click(function() { activePage = reloadTable(--activePage); });
	$( "#save" ).click(function() {
    	var a = {};
    	for (var i = 0; i < localStorage.length; i++) {
        	var k = localStorage.key(i);
        	var v = localStorage.getItem(k);
        	a[k] = v;
    	}
    	var s = JSON.stringify(a);
    	console.log(s);
	});
	
	
	Storage.init();
	UITags.init(function() {
		var knownWords = Storage.map('known').concat(Storage.map('learn')).concat(Storage.map('repeat'));
		for ( var i in tags ) {
			var ws = Lib.intersept(words, tags[i], true);
			var kww = Lib.intersept(ws, knownWords, true);
			if (ws.length && ws.length != kww.length) {
				UITags.append(i, ws.length - kww.length, i==ctag);
			}
		}
		UITags.flush(ctag);
	},  function(tag)    { 	
		ctag = tag;
		activePage = reloadTable(0);
	});
	UITips.init(function( val) { 	pattern = val;  activePage = reloadTable( 0 ); 	});
	UIText.init(function(nwords) { words = nwords; activePage = reloadTable( 0 ); });
	UITables.init(function(word, type, typeEvent, to, elem) {
		switch (typeEvent) {
			case 'move':
				var res = null;
				if ( Storage.types.indexOf(type) > -1 )
					res = Storage.remove(type, word);
				Storage.add(to, word, res ? res.ex : null );
				activePage = reloadTable(activePage);
				break;
			case 'saveEx': Storage.edit( type, word, { ex: UITables.editEx(elem) } ); break;
			case 'editEx': Storage.edit( type, word, { ex: UITables.editEx(elem, true) } ); break;
			case 'up': Storage.edit( type, word, { count: Storage.get(type, Storage.getIndex(type, word)).count + 1 } ); break;
			case 'down': Storage.edit( type, word, { count: Storage.get(type, Storage.getIndex(type, word)).count - 1 } );break;
			default: break;
		}
	});
	reloadTable(0);
});

















