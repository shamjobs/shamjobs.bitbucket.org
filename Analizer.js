Analizer = {
		sep:[ '.' , ',' , '-' , '!', '?', '\"','/','\*', ')', '(', ":", ";", "&", "#", "\r", "\n", ""],
		word: null,	
		regNum: /^\d+$/,
		
		call: function (text) {
			var newWords = {};
			Analizer.analize( text, function(w) {
				w = w.toLowerCase();
				if (newWords[w]) {
					newWords[w]+=1 ;
				} else {
					newWords[w]=1;
				}
			})
			return Object.keys(newWords);
		}, 
		analize: function( text, event ) {
			//text = text.replace(/\&\#\d+\;/g,'');
			this.word = "";
			this.chain = [];
			for ( var i=0; i< text.length; i++ ) {
				var curr=text[i];
				var sep = this.sep.indexOf( curr ) > -1;
				if ( sep || curr==' ') {
					this.newWord(event);
				} else {
					this.word += curr;
				};
			};
			this.newWord(event);
		},	
		
		newWord: function(event) {
			var w = this.normalizeWord(this.word, true);
			if ( w && !this.regNum.test(w) ) event(w);
			this.word = "";
		},
		normalizeWord: function( text, deep ) {
			if (!text) return null;
			var normRes = "";
			text = text.trim();
			var prev = null;
			var lastWord = null;
			//Normalize text
			for ( var i = 0; i< text.length; i++ ) {
				var s = text[i];
				if ( this.checkSeparator(s) ) s = " "; 
				var code = s.charCodeAt(0);
				if ( (code >= 65 && code <= 90) || (code >= 97 && code <= 122)//a-zA-Z  
						|| ( code == 32 && (prev==null || prev != 32) ) //s+
						|| (code >= 48 && code <= 57) //0-9 
						|| ( (code == 39 || code == 45 || code == 8217)  && i >0 && i < text.length -1 )// ' - ’
						) {
					if ( code == 8217 ) {
						code = 39;
						s = "'";
					}
					normRes += s;
					prev = code;
				}
			}
			return normRes;
		},
		
		checkSeparator: function( c ) {
			return this.sep.indexOf( c ) > -1 || c == ' ';
		}
	};